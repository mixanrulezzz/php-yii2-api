<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => env('DB_DRIVER').':host='.env('DB_HOST').';dbname='.env('DB_DATABASE'),
    'username' => env('DB_USERNAME'),
    'password' => env('DB_PASSWORD'),
    'charset' => 'utf8',
];
