# Api

## Методы

### POST /api/add-user/ - Добавление пользователя

Запрос: 
```
{
    "token": "newtokenstring20symb",
    "username": "test"
}
```

`token` - `string`, длина строки 20 букв

`username` - `string`

### POST /api/update-user-projects/ - Обновление списка проектов пользователя

Запрос:
```
{
    "id": 1
}
```

`id` - `integer`, id пользователя

### POST /api/update-project/

Запрос:
```
{
    "id": 12345678,
    "user_id": 1234568,
    "estimate_time": 150
}
```

`id` - `integer`, id проекта

`user_id` - `integer`, id пользователя

`estimate_time` - `integer`, планируемое время

### POST /api/update-issues/

Запрос:
```
{
    "user_id": 5,
    "issues": [
        {
            "id": 123456,
            "project_id": 12345678,
            "cluster_name": "name",
            "estimate_time": 10
        },
        {
            "id": 12345678,
            "project_id": 123456,
            "cluster_name": "name",
            "estimate_time": 12
        }
    ]
}
```

`user_id` - `integer`, id пользователя

`issues` - массив с задачами, которые нужно изменить

`issues.id` - `integer`, id задачи

`issues.project_id` - `integer`, id проекта, к которому принадлежит задача

`issues.cluster_name` - `string`, Название кластера

`issues.estimate_time` - `integer`, Предпологаемое время выполнения

В ответ приходят те задачи, которые смогли обновиться

### POST /api/get-project-issues/

Запрос:
```
{
    "id": 5,
    "project_name": "Gitlab"
}
```

`id` - `integer`, id пользователя

`project_name` - `string`, Название проекта