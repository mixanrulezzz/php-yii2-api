<?php

namespace app\models;

use yii\db\ActiveRecord;

class Issue extends ActiveRecord
{
    public static function tableName()
    {
        return 'issue';
    }
}