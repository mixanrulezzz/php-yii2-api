<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m181028_150027_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'user_id' => $this->integer()->notNull(),
            'estimate_time' => $this->integer()
        ]);
        $this->addForeignKey("fk_project_user_id", "project", "user_id", "user", "id", 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project');
    }
}
