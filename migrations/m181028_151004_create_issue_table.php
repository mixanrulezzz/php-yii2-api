<?php

use yii\db\Migration;

/**
 * Handles the creation of table `issue`.
 */
class m181028_151004_create_issue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('issue', [
            'id' => $this->primaryKey(),
            'issue_id' => $this->integer()->notNull(),
            'name' => $this->text(),
            'desc' => $this->text(),
            'project_id' => $this->integer()->notNull(),
            'spent_time' => $this->integer(),
            'estimate_time' => $this->integer(),
            'cluster_name' => $this->text()
        ]);
        $this->addForeignKey("fk_issue_project_id", "issue", "project_id", "project", "id", 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('issue');
    }
}
