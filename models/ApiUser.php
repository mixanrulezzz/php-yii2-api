<?php

namespace app\models;

use yii\db\ActiveRecord;

class ApiUser extends ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }
}