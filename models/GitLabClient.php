<?php
/**
 * Created by PhpStorm.
 * User: Karpov
 * Date: 27.10.2018
 * Time: 18:35
 */

namespace app\models;


class GitLabClient
{
    const API_URL = 'https://gitlab.com/api/v4';

    private $token;

    public function __construct($token = '')
    {
        $this->token = $token;
    }

    /**
     * @param $token
     * @param $username
     * @link https://docs.gitlab.com/ee/api/projects.html#list-user-projects
     */
    public function getUserProjects($username)
    {
        $url = self::API_URL . "/users/{$username}/projects";

        $result = $this->get($url);

        return $result;
    }

    public function getProjectIssues($projectId)
    {
        $url = self::API_URL . "/projects/{$projectId}/issues";

        $result = $this->get($url);

        return $result;
    }

    public function getUser($username)
    {
        $url = self::API_URL . "/users?username={$username}";

        $result = $this->get($url);

        return $result;
    }

    /**
     * @param $url
     * @return array
     */
    private function get($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($this->token != '') {
            curl_setopt($ch, CURLOPT_HEADER, "Private-Token: {$this->token}");
        }

        $res = curl_exec($ch);

        if(curl_errno($ch))
        {
            $result['success'] = false;
            $result['message'] = 'Ошибка curl в методе ' . __METHOD__ . ': ' . curl_error($ch);
            curl_close($ch);
            return $result;
        }

        curl_close($ch);

        $res = json_decode($res, true);

        $result['success'] = true;
        $result['message'] = 'OK';
        $result['data'] = $res;

        return $result;
    }
}