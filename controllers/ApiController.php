<?php

namespace app\controllers;

use app\models\ApiUser;
use app\models\GitLabClient;
use app\models\Issue;
use app\models\Project;
use yii\db\ActiveRecord;
use yii\web\Controller;
use yii\web\Response;

class ApiController extends Controller
{
    public $enableCsrfValidation = false;
    public $result;

    public function beforeAction($action)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        \Yii::$app->response->format = Response::FORMAT_HTML;
        $this->layout = 'main';
        return $this->render('index');
    }

    public function actionAddUser()
    {
        $client = new GitLabClient('');
        $post = \Yii::$app->request->post();
        if (empty($post['token']) || empty($post['username'])) {
            $this->result['message'] = 'Обязательные поля не заполнены';
            return $this->result;
        }

        if (strlen($post['token']) != 20) {
            $this->result['message'] = 'Неверный токен';
            return $this->result;
        }

        $response = $client->getUser($post['username']);

        if(count($response['data']) == 0) {
            $this->result['message'] = 'Пользователь не найден';
            return $this->result;
        }

        /** @var ActiveRecord $user */
        $user = ApiUser::findOne(['username' => $post['username']]);

        if(!empty($user)) {
            $user->token = $post['token'];
            $user->save();
        } else {
            $user = new ApiUser();
            $user->username = $post['username'];
            $user->token = $post['token'];
            $user->save();
        }

        $res = $this->updateProjects($user);
        if ($res['success'] == false) {
            $this->result['success'] = $res['success'];
            $this->result['message'] = $res['message'];
            $this->result['data'] = [];
            $user->delete();
            return $this->result;
        }

        $this->result = $user;

        return $this->result;
    }

    public function actionUpdateUserProjects()
    {
        $post = \Yii::$app->request->post();
        if (empty($post['id'])) {
            $this->result['message'] = 'Обязательные поля не заполнены';
            return $this->result;
        }

        /** @var ActiveRecord $user */
        $user = ApiUser::findOne(['id' => $post['id']]);

        if(empty($user)) {
            $this->result['message'] = "Такого пользователя не существует";
            return $this->result;
        }

        Project::deleteAll(['user_id' => $user->id]);

        $this->updateProjects($user);
    }

    public function actionGetProjects()
    {
        $post = \Yii::$app->request->post();
        if (empty($post['id'])) {
            $this->result['message'] = 'Обязательные поля не заполнены';
            return $this->result;
        }

        /** @var ActiveRecord $user */
        $user = ApiUser::findOne(['id' => $post['id']]);

        if(empty($user)) {
            $this->result['message'] = "Такого пользователя не существует";
            return $this->result;
        }

        $projects = Project::find()->where(['user_id' => $user->id])->all();

        $this->result = $projects;

        return $this->result;
    }

    public function actionGetProjectIssues()
    {
        $post = \Yii::$app->request->post();
        if (empty($post['id']) || empty($post['project_name'])) {
            $this->result['message'] = 'Обязательные поля не заполнены';
            return $this->result;
        }

        /** @var ActiveRecord $user */
        $user = ApiUser::findOne(['id' => $post['id']]);

        if (empty($user)) {
            $this->result['message'] = "Такого пользователя не существует";
            return $this->result;
        }

        $project = Project::findOne(['name' => $post['project_name'], 'user_id' => $user->id]);

        if (empty($project)) {
            $this->result['message'] = "Такого проекта не существует";
            return $this->result;
        }

        $issues = Issue::find()->where(['project_id' => $project->id])->all();

        $this->result= $issues;

        return $this->result;
    }

    public function actionGetAllIssues()
    {
        $post = \Yii::$app->request->post();
        if (empty($post['id'])) {
            $this->result['message'] = 'Обязательные поля не заполнены';
            return $this->result;
        }

        /** @var ActiveRecord $user */
        $user = ApiUser::findOne(['id' => $post['id']]);

        if (empty($user)) {
            $this->result['message'] = "Такого пользователя не существует";
            return $this->result;
        }

        $projects = Project::find()->where(['user_id' => $user->id])->all();

        $issues = [];
        foreach ($projects as $project) {
            $issues = Issue::find()->where(['project_id' => $project->id])->all();
        }

        $this->result['data'] = [
            'projects' => $projects,
            'issues' => $issues
        ];

        return $this->result;
    }

    public function actionUpdateProject()
    {
        $post = \Yii::$app->request->post();
        if (empty($post['id']) || empty($post['user_id']) || empty($post['estimate_time'])) {
            $this->result['message'] = 'Обязательные поля не заполнены';
            return $this->result;
        }
        if (!is_integer($post['id']) || !is_integer($post['user_id']) || !is_integer($post['estimate_time'])) {
            $this->result['message'] = 'Все поля должны быть целочисленными';
            return $this->result;
        }

        $project = Project::findOne(['user_id' => $post['user_id'], 'id' => $post['id']]);

        if (empty($project)) {
            $this->result['message'] = 'Проекта с id ' . $post['id'] . ' для пользователя с id ' . $post['user_id'] . ' не существует';
            return $this->result;
        }

        $project->estimate_time = $post['estimate_time'];
        $project->save();

        $this->result['data'] = $project;

        return $this->result;
    }

    public function actionUpdateIssues()
    {
        $post = \Yii::$app->request->post();
        if (empty($post['user_id']) || empty($post['issues']) || !is_array($post['issues'])) {
            $this->result['message'] = 'Обязательные поля не заполнены';
            return $this->result;
        }
        if (!is_integer($post['user_id'])) {
            $this->result['message'] = 'Поле user_id должно быть целочисленным';
            return $this->result;
        }
        $issues = [];

        foreach ($post['issues'] as $issueData) {
            if (empty($issueData['id']) ||
                empty($issueData['project_id']) ||
                empty($issueData['cluster_name']) ||
                empty($issueData['estimate_time'])) {

                $this->result['message'] = 'Не хватает обязательных полей в масиве issues';
                return $this->result;
            }

            $project = Project::findOne(['id' => $issueData['project_id'], 'user_id' => $post['user_id']]);
            if (empty($project)) {
                continue;
            }

            $issue = Issue::findOne(['id' => $issueData['id'], 'project_id' => $issueData['project_id']]);
            if (empty($issue)) {
                continue;
            }

            $issue->cluster_name = $issueData['cluster_name'];
            $issue->estimate_time = $issueData['estimate_time'];
            $issue->save();
            $issues[] = $issue;
        }

        $this->result['data'] = [
            "issues" => $issues
        ];

        return $this->result;
    }

    private function updateProjects($user)
    {
        $client = new GitLabClient($user->token);
        $response = $client->getUserProjects($user->username);
        $result['success'] = true;
        $result['message'] = 'OK';

        if(!$response['success'] || !is_array($response['data'])) {
            $result['success'] = false;
            $result['message'] = $response['message'];
            return $result;
        }

        foreach ($response['data'] as $item) {
            if (!is_integer($item['id'])) {
                $result['message'] = 'Ошибка при обновлении проектов';
                $result['success'] = false;
                return $result;
            }
            $project = Project::findOne($item['id']);
            if (!empty($project)) {
                $project->name = $item['name'];
                $project->user_id = $user->id;
                $project->save();
            } else {
                $project = new Project();
                $project->id = $item['id'];
                $project->name = $item['name'];
                $project->user_id = $user->id;
                $project->save();
            }

            $issueResponse = $client->getProjectIssues($item['id']);
            if ($issueResponse['success']) {
                foreach ($issueResponse['data'] as $datum){
                    $issue = Issue::findOne($datum['id']);
                    if (!empty($issue)) {
                        $issue->name = $datum['title'];
                        $issue->desc = $datum['description'];
                        //$issue->spent_time = $datum['time_stats']['total_time_spent'];
                        //$issue->estimate_time = $datum['time_stats']['time_estimate'];
                        $issue->save();
                    } else {
                        $issue = new Issue();
                        $issue->id = $datum['id'];
                        $issue->issue_id = $datum['iid'];
                        $issue->name = $datum['title'];
                        $issue->desc = $datum['description'];
                        $issue->project_id = $datum['project_id'];
                        $issue->spent_time = $datum['time_stats']['total_time_spent'];
                        $issue->estimate_time = $datum['time_stats']['time_estimate'];
                        $issue->save();
                    }
                }
            }
        }

        return $result;
    }
}